# ItemChecksum

## Description
ItemChecksum is a module for [Gallery 3](http://gallery.menalto.com/) which will display the MD5 or SHA-1 checksum of the specified item (picture or video). 

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/90270).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.

Once installed, you will have access to the following URLs -- 
 - For MD5 Checksum's, the URL is: http://www.example.com/gallery/index.php/itemchecksum/md5/ALBUM_ID_NUM/FILE_NAME
 - For SHA1 Checksum's, the URL is: http://www.example.com/gallery/index.php/itemchecksum/sha1/ALBUM_ID_NUM/FILE_NAME

To determine the number of non-album items in an album, the URL is: http://www.example.com/gallery/index.php/itemchecksum/albumcount/20

The following URLs will return REST-friendly SHA1 and MD5 checksums:
 - http://www.example.com/gallery3/index.php/rest/itemchecksum_sha1/ITEM_#
 - http://www.example.com/gallery3/index.php/rest/itemchecksum_md5/ITEM_#

## History
**Version 1.3.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Bug Fix:  Updated REST API integration to generate correct MD5 and SHA1 URLs when requesting details for an item.
> - Released 06 November 2021.
>
> Download: [Version 1.3.0](/uploads/fb981c295d5d16381f2c24113be09034/itemchecksum130.zip)

**Version 1.2.1:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 08 August 2011. 
>
> Download: [Version 1.2.1](/uploads/80eaef69726e276ec8958011cb622930/itemchecksum121.zip)

**Version 1.2.0:**
> - Updated to include REST support.
> - Released 29 October 2010.
>
> Download: [Version 1.2.0](/uploads/d03d4ac83920578b96bc6d1b143fc90a/itemchecksum120.zip)

**Version 1.1.0:**
> - Updated for recent changes in Gallery 3's module API.
> - Released 20 January 2010.
>
> Download: [Version 1.1.0](/uploads/76a8dbc6f5a17514dbfba4e4e5d2ad6d/itemchecksum110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 20 August 2009.
>
> Download: [Version 1.0.0](/uploads/f1cf16c68956651c3a1a5790e59b5258/itemchecksum100.zip)
